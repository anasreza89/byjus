import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {Observable,of, from } from 'rxjs';
import { Course } from './interface';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  private url:string = 'https://api.myjson.com/bins/1fq8pm';

  constructor(private http: HttpClient) { }

  getList(): Observable<Course[]>{
    return this.http.get<Course[]>(this.url);
  }

}
