import { Component, OnInit } from '@angular/core';
import { CategoryService } from './../category.service';
import {Sort} from '@angular/material';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  public count;
  public filterCount;
  public dataList = [];
  public filterResult = [];
  public provider = ["Udacity", "Coursera", "Canvas.net"];
  public parentSubject = ["Computer Science", "Programming", "Art & Design"];
  public childSubject = ["Artificial Intelligence", "Databases", 'Web Development'];

  constructor(private categoryService: CategoryService ) { }

  ngOnInit() {

    //on load call service to populate datalist
    //one time call, reducing http activity several times
    this.categoryService.getList()
      .subscribe(data => {
            this.count = data.length;
            this.dataList = data;
            this.filterResult = this.dataList
          }
        );
  }

  //filter data list on selection of dropdown value
  filterForeCasts(filterVal: any) {
      if(filterVal == "0")
        this.dataList = this.filterResult ;
      else
      this.dataList = this.filterResult.filter((item) => Object.keys(item).some(k => item[k] == filterVal));

      this.filterCount = this.dataList.length;
  };

  // sort datalist on selection of length column
  sortData(sort: Sort) {
    const data = this.dataList.slice();

    if (!sort.active || sort.direction === '') {
      this.dataList = this.dataList;
      return;
    }

    this.dataList = this.dataList.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'Length': return this.compare(a.Length, b.Length, isAsc);
        default: return 0;
      }
    });

  }

  // compare the values of length of two rows
  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

}
