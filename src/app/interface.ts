export interface Course{
  subject: string,
  id: number,
  courseName: string,
  length: number,
  date: string,
  pSubject: string,
  provider: string,
  university: string
}
